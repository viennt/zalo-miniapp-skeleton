import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from 'path';
import topLevelAwait from "vite-plugin-top-level-await";
import federation from "@originjs/vite-plugin-federation";
import getRoutes from "./src/helper/routes"

console.log(getRoutes('config'))

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    topLevelAwait({
      // The export name of top-level await promise for each chunk module
      promiseExportName: "__tla",
      // The function to generate import names of top-level await promise in each chunk module
      promiseImportName: i => `__tla_${i}`
    }),
    vue(),
    federation({
      name: 'remote-app',
      filename: 'remoteEntry.js',
      // Modules to expose
      // exposes: {
      //     './App': './src/App.vue',
      //     './home': './src/pages/Home/home.vue',
      //     './detail': './src/pages/EventDetail/index.vue',
      //     './order': './src/pages/Order/index.vue',
      //     './order-create': './src/pages/Order/create.vue',
      //     './order-import': './src/pages/Order/import.vue',
      //     './profile': './src/pages/Profile/index.vue',
      //     './profile-edit': './src/pages/Profile/edit.vue',
      //     './search': './src/pages/Search/index.vue',
      //     './statistic': './src/pages/Statistic/index.vue'
      // },
      exposes: {
        './main': "./src/main",
        './app': "./src/App.vue",
        './setup': "./src/setup",
        './pageSetup': "./src/pages/pageSetup.vue",
        ...getRoutes('config')
      },
      remotes: {
        remote_app: "http://localhost:4001/assets/remoteEntry.js",
      },
      shared: ['vue']
    })
  ],
  build: {
    outDir: 'dist',
    rollupOptions: {
      output: {
        entryFileNames: `assets/[name].module.js`,
        chunkFileNames: `assets/[name].module.js`,
        assetFileNames: `assets/[name].[ext]`
      }
    },
    // target: ["chrome89", "edge89", "firefox89", "safari13"]
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
      '#root': resolve(__dirname)
    }
  },
  server: {
    port: 4001
  },
  preview: {
    port: 4001
  },
});
