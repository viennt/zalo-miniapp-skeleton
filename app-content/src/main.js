import { createApp, defineAsyncComponent } from "vue";
import App from "./App.vue";
import router from "./helper/router";

window.app = createApp(App).use(router);

const remoteApp = () => import("@/setup");
remoteApp().then(lib => {
  setTimeout(() => {
    window.app.mount("#app");
  },10)
})
