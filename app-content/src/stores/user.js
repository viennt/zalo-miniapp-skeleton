import { defineStore } from "pinia";

const schema = {
  id: "string",
  avatar: "string",
  name: "string",
  zid: "string",
  description: "string",
  nodeId: "string",
  category: "string[]",
  language: "string[]",
  storage: "number",
  phone: "string",
  bank: "string"
}

export const userStore = defineStore("user", {
  state: () => {
    const user = {};
    Object.keys(schema).forEach(k => user[k] = null);
    return user;
  },
  getters: {
    // doubleCount: (state) => state.count * 2,
  },
  actions: {
    update(userInfo) {
      Object.keys(schema).forEach(k => {
        this[k] = userInfo[k]
      })
      this.id = userInfo._id;
    }
  }
});
