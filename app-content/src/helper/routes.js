const routes = [
	{
		path: "/home",
		name: "home",
		component: () => import("@/pages/Home/index.vue"),
		remotePath: "./src/pages/Home/index.vue",
		remoteComponent: () => import("remote_app/home")
	},
];

function getRoutes(mode='direct') {
	if ( mode=='direct' ) 
		return routes.map(r => ({
			path: r.path,
			component: r.component,
		}));
	if ( mode=='remote' )
		return routes.map(r => ({
			path: r.path,
			component: r.remoteComponent
		}));
	if ( mode=='config' ) {
		const routeMap = {};
		routes.forEach(r => {
			routeMap['./'+r.name] = r.remotePath;
		})
		return routeMap;
	}
}

export default getRoutes;