import { userStore } from "../stores/user";

const stores = {};
const setupStores = () => {
  if (window.stores) {
    window.location.reload();
  }
  Object.assign(stores, {
    user: userStore()
  });
  window.stores = stores;
};

// Promise.resolve().then(setupStores);
setTimeout(setupStores)

export default {
  install: (app, options) => {
    app.config.globalProperties.$stores = stores;
  }
};
