# Vue 3 + Vite + Federation plugin

Dự án gồm 2 thành phần
- App Cover chỉ bao gồm phần vỏ bọc, phần này sẽ load phần nội dung chính từ 1 URL khác
- App Content chứa phần nội dung chính của app

Sở dĩ cần chia ra 2 phần như trên là vì Zalo miniapp yêu cầu kiểm duyệt app trước khi lên store. Và phần kiểm duyệt này thường lâu trong khi chúng ta muốn cập nhật ứng dụng ngay. Do đó, với khung phần mềm như trên, chúng ta chỉ cần upload phần App Cover lên Zalo, còn phần App Content sẽ được host trên server của chúng ta và có thể sửa đổi cập nhật bất cứ lúc nào.

# Dev

```
cd app-content
pnpm run dev
```

# Preview

```
pnpm run build && pnpm run preview
```

