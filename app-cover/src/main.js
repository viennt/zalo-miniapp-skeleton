import { createApp, defineAsyncComponent } from "vue";
import App from "./App.vue";
import router from "./router";

window.app = createApp(App).use(router);

const remoteApp = () => import("remote_app/setup");
remoteApp().then(lib => {
  setTimeout(() => {
    window.app.mount("#app");
  },10)
})
