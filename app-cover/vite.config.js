import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from 'path';
import topLevelAwait from "vite-plugin-top-level-await";
import federation from "@originjs/vite-plugin-federation";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    topLevelAwait({
      // The export name of top-level await promise for each chunk module
      promiseExportName: "__tla",
      // The function to generate import names of top-level await promise in each chunk module
      promiseImportName: i => `__tla_${i}`
    }),
    vue(),
    federation({
      name: 'host-app',
      remotes: {
        remote_app: "http://localhost:4001/assets/remoteEntry.js",
      },
      shared: ['vue']
    })
  ],
  build: {
    outDir: 'dist',
    rollupOptions: {
      output: {
        entryFileNames: `[name].module.js`,
        chunkFileNames: `[name].module.js`,
        assetFileNames: `[name].[ext]`
      }
    },
    // target: ["chrome89", "edge89", "firefox89", "safari13"]
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
      '#root': resolve(__dirname)
    }
  },
  preview: {
    port: 4000
  }
});
